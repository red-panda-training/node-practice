import mongoose from "mongoose";

const userSchema = mongoose.Schema({
  fullname: String,
  email: String,
  username: String,
  password: String,
  creationDate: {
    type: Date,
    default: new Date(),
  },
  modifiedDate: {
    type: Date,
    default: null,
  },
});

const Users = mongoose.model("users", userSchema);
export default Users;
