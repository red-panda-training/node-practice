import express from "express";
import cors from "cors";
import bodyParser from "body-parser";
import userRoutes from "./routes/users.js";
import mongoose from "mongoose";
import { Config } from "./utils/config.js";

const app = express();
app.use(cors());
app.use(bodyParser.json({ limit: "6mb" }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use("/user", userRoutes);

app.get("/", (req, res) => {
  res.send(`<h1>Welcome to node js practice</h1>`);
});

app.get("/ping", (req, res) => {
  res.send(`<h1>This app is running on port ${Config.PORT}</h1>`);
});

const CONNECTION_URL = Config.CONNECTION_URL;

export default mongoose
  .connect(CONNECTION_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    app.listen(Config.PORT, () => {
      console.log(`Server running on PORT : ${Config.PORT}`);
    });
  })
  .catch((error) => {
    console.log(error.message);
  });
