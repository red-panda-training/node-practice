import dotenv from "dotenv";

dotenv.config();

export const Config = {
  PORT: process.env.PORT || 8001,
  CONNECTION_URL: process.env.CONNECTION_URL,
};
