import axios from "axios";

const serverBaseURL = process.env.BASE_URL;
const axiosInterceptorInstance = axios.create({
  baseURL: serverBaseURL,
  headers: {
    "Content-Type": "application/json",
  },
});

// Request interceptor
axiosInterceptorInstance.interceptors.request.use(
  (config) => {
    const request = config;
    const authToken = localStorage.getItem("node_acc_token");
    if (authToken) {
      if (config.headers === undefined) {
        request.headers = {};
      }
      request.headers.Authorization = `Bearer ${authToken}`;
    }
    return request;
  },
  (error) => {
    return Promise.reject(error);
  }
);

// Response interceptor

axiosInterceptorInstance.interceptors.response.use(
  (config) => {
    return config;
  },
  (error) => {
    if (error?.response?.status === 401) {
      localStorage.clear();
      window.location.replace("/");
    }
    return Promise.reject(error);
  }
);

export default axiosInterceptorInstance;
