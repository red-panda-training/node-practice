import express from "express";
import { getUsers, createUser, loginUser } from "../controllers/users.js";
import { authenticateUser } from "../utils/middleware.js";

const router = express.Router();

router.get("/allusers", authenticateUser, getUsers);
router.post("/signup", createUser);
router.post("/login", loginUser);

export default router;
