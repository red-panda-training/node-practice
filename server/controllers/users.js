import mongoose from "mongoose";
import express from "express";
import Users from "../models/users.js";
import Bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
const router = express.Router();

export const getUsers = async (req, res) => {
  try {
    const users = await Users.find();
    return res.status(200).json({ status: "success", data: users });
  } catch (error) {
    return res.status(404).json({ status: "failure", message: error.message });
  }
};

export const createUser = async (req, res) => {
  try {
    const { fullname, username, password, email } = req.body;
    const emailExists = await Users.find({ email: email });
    if (emailExists.length) {
      return res
        .status(401)
        .json({ status: "failure", message: "Email-Exists" });
    }
    const usernameExists = await Users.find({ username: username });
    if (usernameExists.length) {
      return res
        .status(401)
        .json({ status: "failure", message: "Username-Exists" });
    }
    const hashedPassword = await Bcrypt.hash(password, 10);
    const user = new Users({
      fullname,
      username,
      password: hashedPassword,
      email,
    });
    await user.save();
    return res.status(200).json({ status: "success", data: user });
  } catch (error) {
    return res.status(404).json({ status: "failure", message: error.message });
  }
};

export const loginUser = async (req, res) => {
  try {
    const { username, password } = req.body;
    console.log(req.body);
    const usernameExists = await Users.find({ username: username });
    if (!usernameExists.length) {
      return res
        .status(401)
        .json({ status: "failure", message: "Invalid-credentials" });
    }
    const match = Bcrypt.compare(password, usernameExists[0].password);
    if (!match) {
      return res
        .status(401)
        .json({ status: "failure", message: "Invalid-credentials" });
    }
    const accessToken = jwt.sign(
      { name: username },
      process.env.ACCESS_TOKEN_SECRET
    );
    return res.json({ status: "success", accessToken: accessToken });
  } catch (error) {
    return res.status(404).json({ status: "failure", message: error.message });
  }
};

export default router;
